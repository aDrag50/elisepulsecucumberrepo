
Feature: Hello World
  <Some description>

  Scenario: New Test Case
    Given I open the app
    When I click something
    Then I expect something else to show up
  
  Scenario: Verify homepage redirect
    Given I open the app
    When I log in
    Then I should be redirected to the dashboard
